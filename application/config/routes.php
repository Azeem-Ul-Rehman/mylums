<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'user/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* CMS */
$route['terms-and-conditions'] 				= 'cms/terms_and_conditions';
$route['privacy-policy'] 					= 'cms/privacy_policy';
$route['page/(:any)'] 						= 'cms/index';


/*admin*/
$route['admin'] 							= 'user/index';
$route['admin/signup'] 						= 'user/signup';
$route['admin/create_member'] 				= 'user/create_member';
$route['admin/login'] 						= 'user/index';
$route['admin/logout'] 						= 'user/logout';
$route['admin/login/validate_credentials'] 	= 'user/validate_credentials';

/* alumni */
$route['admin/alumni'] 						= 'admin/alumni/index';
$route['admin/alumni/add'] 					= 'admin/alumni/add';
$route['admin/alumni/update'] 				= 'admin/alumni/update';
$route['admin/alumni/upload'] 				= 'admin/alumni/uploadfile';
$route['admin/alumni/preload'] 				= 'admin/alumni/preload';
$route['admin/alumni/update/(:any)'] 		= 'admin/alumni/update/$1';
$route['admin/alumni/delete/(:any)'] 		= 'admin/alumni/delete/$1';
$route['admin/alumni/(:any)'] 				= 'admin/alumni/index/$1'; //$1 = page number


/* industry */
$route['admin/industry'] 					= 'admin/industry/index';
$route['admin/industry/add'] 				= 'admin/industry/add';
$route['admin/industry/update'] 			= 'admin/industry/update';
$route['admin/industry/update/(:any)'] 		= 'admin/industry/update/$1';
$route['admin/industry/delete/(:any)'] 		= 'admin/industry/delete/$1';
$route['admin/industry/(:any)'] 			= 'admin/industry/index/$1'; //$1 = page number

/* interests */
$route['admin/interests'] 					= 'admin/interests/index';
$route['admin/interests/add'] 				= 'admin/interests/add';
$route['admin/interests/update'] 			= 'admin/interests/update';
$route['admin/interests/update/(:any)']		= 'admin/interests/update/$1';
$route['admin/interests/delete/(:any)'] 	= 'admin/interests/delete/$1';
$route['admin/interests/(:any)'] 			= 'admin/interests/index/$1'; //$1 = page number

/* events */
$route['admin/events/setlocation']		= 'admin/events/setlocation';
$route['admin/events'] 					= 'admin/events/index';
$route['admin/events/add'] 				= 'admin/events/add';
$route['admin/events/update'] 			= 'admin/events/update';
$route['admin/events/update/(:any)'] 	= 'admin/events/update/$1';
$route['admin/events/delete/(:any)'] 	= 'admin/events/delete/$1';
$route['admin/events/(:any)'] 			= 'admin/events/index/$1'; //$1 = page number

/* partners */
$route['admin/partners'] 					= 'admin/partners/index';
$route['admin/partners/add'] 				= 'admin/partners/add';
$route['admin/partners/update'] 			= 'admin/partners/update';
$route['admin/partners/update/(:any)'] 		= 'admin/partners/update/$1';
$route['admin/partners/delete/(:any)'] 		= 'admin/partners/delete/$1';
$route['admin/partners/(:any)'] 			= 'admin/partners/index/$1'; //$1 = page number


/* offers */
$route['admin/offers'] 						= 'admin/offers/index';
$route['admin/offers/add'] 					= 'admin/offers/add';
$route['admin/offers/update'] 				= 'admin/offers/update';
$route['admin/offers/update/(:any)']		= 'admin/offers/update/$1';
$route['admin/offers/delete/(:any)'] 		= 'admin/offers/delete/$1';
$route['admin/offers/(:any)'] 				= 'admin/offers/index/$1'; //$1 = page number


/* CMS */
$route['admin/cms'] 						= 'admin/cms/index';
$route['admin/cms/add'] 					= 'admin/cms/add';
$route['admin/cms/update'] 					= 'admin/cms/update';
$route['admin/cms/update/(:any)']			= 'admin/cms/update/$1';
$route['admin/cms/delete/(:any)'] 			= 'admin/cms/delete/$1';
$route['admin/cms/(:any)'] 					= 'admin/cms/index/$1'; //$1 = page number

//API Users

$route['signup'] 							= 'preload/signup';
$route['login'] 							= 'auth/token';
$route['logout'] 							= 'users/logout';
$route['forgotpassword'] 					= 'preload/forgot_password';
$route['users/profile/update']				= 'users/profile_update';
//$route['users/profile/qrcode']				= 'users/profile_qrcode';
$route['users/profile/qrcode/(:any).png']	= 'auth/profile_qrcode/$1';
$route['verification/(:any)'] 				= 'preload/verification';
$route['options/homescreen'] 				= 'auth/homescreen';

/* alumni API */
$route['alumni/profile/(:any)']				= 'users/profile/$1';
$route['industry/list']						= 'industry/all';

// messages/threads
$route['messages/threads/(:any)']						= 'messages/threads/$1';
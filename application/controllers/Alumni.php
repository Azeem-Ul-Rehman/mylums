<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
// Alumni Api

require('Api.php');

class Alumni extends Api
{
    public $content_type = "application/json";
    public $columns;
    public $columnsEdit;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function all()
    {
        if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
            $this->api_model->response('',406);
        } else {
            $data['data'] = array();
            $data['status'] = 'Error';

            //if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
            {
                $path = base_url('/uploads/');
                $profile = base_url('/alumni/profile/');
                $qrcode = base_url() .'users/profile/qrcode/';
                $this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*, 
								  CONCAT(\''.$qrcode. '\', users.id) as qrcode,
								  CONCAT(\''.$path. '\', profile.image) as imagepath, 
								  CONCAT(\''.$profile. '\', users.id) as profilepath');
                //$this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*');
                $this->db->where('users.id = profile.user_id');
                $this->db->where('users.user_type != "admin"');
                $this->db->where('users.status', "active");
                $this->db->from('users,profile');

                $limit = 10;
                $paged = $this->input->get('paged');
                $offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
                //$this->db->limit($limit, $offset);

                $query = $this->db->get();
                $last_query = $this->db->last_query();

                $data['total'] = $query->num_rows();
                $data['pages'] = ceil($query->num_rows()/$limit);

                if($this->input->get('all') == 'yes')
                {
                    $data2 = $query;
                } else {
                    $data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
                }

                //$this->db->limit($limit, $offset);
                //var_dump($data2->result_array());

                $data['data'] = $data2->result_array();

                /*if(count($data['data']) > 0)
                {
                    foreach($data['data'] as &$row)
                    {
                        $test = $this->user_model->get_user_object($row['user_id']);
                        $row = $test['user'];
                    }
                }*/

                //var_dump($data['data']);
                if(count($data['data']) > 0)
                {
                    $data['status'] = 'Successful';
                    $data['message'] = 'Alumni list.';
                } else {
                    $data['status'] = 'Error';
                    $data['message'] = 'Alumni not found.';
                    $this->api_model->response($this->json->encode($data),406);
                }
                $this->api_model->response($this->json->encode($data),200);
            }
            $this->api_model->response($this->json->encode($data),404);

        }
    }

    public function search()
    {
        if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
            $this->api_model->response('',406);
        } else {
            $data['data'] = array();
            $data['status'] = 'Error';

            $search = $this->input->get('q');
            $advance = $this->input->get('advance');

            //if($search)
            {
                $path = base_url('/uploads/');
                $profile = base_url('/alumni/profile/');
                $qrcode = base_url() .'users/profile/qrcode/';
                //$this->db->select('*');
                $this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*, 
								  CONCAT(\''.$qrcode. '\', users.id) as qrcode,
								  CONCAT(\''.$path. '\', profile.image) as imagepath, 
								  CONCAT(\''.$profile. '\', users.id) as profilepath');

                if($advance == 'yes')
                {
                    if($search)
                    {
                        $this->db->group_start();
                        $this->db->or_like('users.roll_number',$search);
                        $this->db->or_like('first_name',$search);
                        $this->db->or_like('last_name',$search);
                        $this->db->group_end();
                    }

                    $statusCheck = false;

                    $this->db->group_start();
                    if($this->input->get('program'))
                    {
                        $statusCheck = true;
                        $this->db->like('program',$this->input->get('program'));
                    }
                    if($this->input->get('company'))
                    {
                        $statusCheck = true;
                        $this->db->like('current_company',$this->input->get('company'));
                    }
                    if($this->input->get('designation'))
                    {
                        $statusCheck = true;
                        $this->db->like('current_designation',$this->input->get('designation'));
                    }
                    if($this->input->get('department'))
                    {
                        $statusCheck = true;
                        $this->db->like('current_department',$this->input->get('department'));
                    }
                    if($this->input->get('country'))
                    {
                        $statusCheck = true;
                        $this->db->like('country',$this->input->get('country'));
                    }

                    if($this->input->get('state_province'))
                    {
                        $statusCheck = true;
                        $this->db->like('state_province',$this->input->get('state_province'));
                    }

                    if($this->input->get('city'))
                    {
                        $statusCheck = true;
                        $this->db->where('city',$this->input->get('city'));
                    }
                    $this->db->group_end();

                    if($statusCheck == false)
                    {
                        $data['status'] = 'Error';
                        $data['message'] = 'Advanced Search Criteria messing.';
                        $this->api_model->response($this->json->encode($data),406);
                    }

                } else {
                    $this->db->group_start();
                    $this->db->or_like('users.roll_number',$search);
                    $this->db->or_like('first_name',$search);
                    $this->db->or_like('last_name',$search);
                    $this->db->group_end();
                }


                $this->db->group_start();
                $this->db->where('users.id = profile.user_id');
                $this->db->where('users.user_type != "admin"');
                $this->db->where('users.status', "active");
                $this->db->group_end();

                $this->db->from('users,profile');

                $limit = 10;
                $paged = $this->input->get('paged');
                $offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
                //$this->db->limit($limit, $offset);

                $query = $this->db->get();
                $last_query = $this->db->last_query();

                $data['total'] = $query->num_rows();
                $data['pages'] = ceil($query->num_rows()/$limit);

                if($this->input->get('all') == 'yes')
                {
                    $data2 = $query;
                } else {
                    $data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
                }

                $this->db->limit($limit, $offset);
                //var_dump($data2->result_array());

                $data['data'] = $data2->result_array();
                //var_dump($data['data']);
                if(count($data['data']) > 0)
                {
                    $data['status'] = 'Successful';
                    $data['message'] = 'Alumni list.';
                } else {
                    $data['status'] = 'Error';
                    $data['message'] = 'Alumni not found.';
                    $this->api_model->response($this->json->encode($data),404);
                }
                $this->api_model->response($this->json->encode($data),200);
            }
            $data['status'] = 'Error';
            $data['message'] = 'Alumni not found.';
            $this->api_model->response($this->json->encode($data),404);

        }
    }
    public function preload($user_id){

        $data = array();

        if ($this->input->server('REQUEST_METHOD') != 'GET')
        {

            $this->api_model->response('',406);

        }else{

            $userid = $this->uri->segment(3);
            $user 				= $this->user_model->profile_get($userid);

            if($user){

                $preload 			= $this->user_model->preload($user['email']);
                $preload['usertype'] = 'preload';
                if($preload){

                    $data['user'] = array_merge($preload,$user );
                    $data['status'] = 'Successfull';
                    $data['message'] = 'User Profile';

                }else{

                    $data['user'] = array();
                    $data['status'] = 'Error';
                    $data['message'] = 'User Preload not found.';

                }
            }else{

                $data['user'] = array();
                $data['status'] = 'Error';
                $data['message'] = 'User not found.';
            }
            $this->api_model->response($this->json->encode($data),200);
        }
    }
}
?>

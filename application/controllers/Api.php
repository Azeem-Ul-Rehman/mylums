<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller 
{
	public $currentUserId = null;
	public function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
		$this->load->model('api_model');
		
		$headers = $this->input->request_headers();
		
		$login = $this->uri->segment('1');
		if($login == 'logout')
		{
			//echo $login; 
			$getToken = $this->user_model->verify_token($headers['Authorization']);
			$token = Authorization::validateToken($headers['Authorization']);
			$this->currentUserId = $token;
			
		} else {
			if (Authorization::tokenIsExist($headers)) 
			{
				$getToken = $this->user_model->verify_token($headers['Authorization']);
				
				if($getToken == null)
				{
					$data = array();
					$data['logout'] = 'yes';
					$data['status'] = 'Error';
					$data['message'] = 'Token has been expired.';
					$this->api_model->response($this->json->encode($data),404);
					return;
				}
				
				$token = Authorization::validateToken($headers['Authorization']);
				$this->currentUserId = $token;
			} else {
				$data = array();
				$data['status'] = 'Error';
				$data['message'] = 'Token id missing.';
				$this->api_model->response($this->json->encode($data),404);
				return;
			}
		}
	}
	
	public function push()
	{
		$fcm = $this->user_model->get_fcm(33);
		echo $fcm;
		die();
		$this->load->model('notification_model','notification');
		$message = array();
		$message['title'] = 'Testing';
		$message['body'] = 'Hello';
		$type_id = '2';
		$type = 'event';
		$id = 'dBKhigUPg0M:APA91bGg6U-qexi4yfCHgzxDg2MEW-JRgEcRynzyM422-LCh_SVgUM7NxZnB7b-ypFOBTw99XyTv3iY7MoqblnBHxwxyVWmN3WkmGERRcpuO1rRtidTVeLsIMaxy-8AI1e4Pp0SdJz0Z';
		
		$data = $this->notification->sendFCM($message,$id,$type,$type_id);
		var_dump($data);
		
		// Message //
		$message = array();
		$message['title'] = 'Ghafar BHai';
		$message['body'] = 'I am back';
		$type_id = '9';
		$type = 'message';
		$id = 'drxR_F3rOO4:APA91bEGzSUHFnC39T4DozhXenovB_PdZfIiG-SZBuaLjjOKoaSbPtvenGXJMcIw7-J7UbyClGNYcOlBtwxTfbIcjuFo8MhPm__ZilBmOlWyFx_rq04m0WUOReCqVrjostmybsi9cPDB';
		$user = $this->user_model->get_user_object(33);
		
		$data = $this->notification->sendFCM($message,$id,$type,$type_id,$user);
		var_dump($data);
	}
}


?>
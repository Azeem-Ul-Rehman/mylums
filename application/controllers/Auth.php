<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Auth extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }
	
	public function index_post()
	{
	
	}
	
	public function index_get()
	{
	
	}

    public function token_post()
    {
        $dataPost = $this->input->post();
		$user = $this->user_model->login($dataPost['username'], password_encode($dataPost['password']));
		//var_dump($user);
        if ($user != null) {
			/*$interests = $this->user_model->interests($user['id']);
			$workexperience = $this->user_model->workexperience($user['id']);
			
			$preload = $this->preload($user['email']);
			$linkedin = $this->user_model->linkedin($user['id']);
			
			$setting = $this->get_setting($user['id']);
			
            
            $response['user'] =  $this->user_model->profile_get($user['id']);
			$response['user']['interests'] = $interests;
			$response['user']['workexperience'] = $workexperience;
			
			$response['user']['proload'] = $preload;
			$response['user']['linkedin'] = $linkedin;
			$response['user']['qrcode'] =  base_url() .'users/profile/qrcode/' . $user['id'] . '.png';
			$response['user']['imagepath'] =  base_url('/uploads/') .$response['user']['image'];
			
			$response['user']['setting'] = $setting;
			
			$response['homescreen'] = $this->homescreen_get('no');*/
			
			$tokenData = array();
            $tokenData['id'] = $user['id'];
			$response = $this->user_model->get_user_object($user['id']);
			
			$response['status'] = 'Successful';
			$response['token'] = Authorization::generateToken($tokenData);
			$this->user_model->update_token($user['id'], $response['token']);
			$this->set_response($response, REST_Controller::HTTP_OK);
            return;
        }
        $response = [
            'status' => 'Error',
            'message' => 'Unauthorized',
        ];
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    }
	
	public function homescreen_get($output = NULL)
	{
		$arraylist = array();
		$this->db->where('option_name','homescreen');
		$ListTypes = $this->db->get('options');
		$ListTypes = $ListTypes->row_array();
		$path = base_url('/uploads/') . $ListTypes['option_value'];
		
		if($output == 'no')
		{
			return $path;
		} else {
			$response['homescreen'] = $path;
			$response['status'] = 'Successful';
			$this->set_response($response, REST_Controller::HTTP_OK);
		}
	}
	
	public function profile_qrcode_get($id = NULL)
	{
			header("Content-Type: image/png");
			//echo $id;
			$userID = $this->currentUserId->id;
			$userID = $id;
			$userData = $this->user_model->profile_get($userID);
			
			$response['user']['id'] = $userData['user_id'];
			$response['user']['name'] = $userData['first_name'] . ' '.$userData['last_name'] ;
			$response['user']['email'] = $userData['email'];
			$response['user']['phone'] = $userData['phone'];
			
			$response['user']['company'] = $userData['current_company'];
			$response['user']['industry'] = $userData['current_industry'];
			$response['user']['designation'] = $userData['current_designation'];
			$response['user']['department'] = $userData['current_department'];
			
			$QRCode = base_url('qrcode/index.php/?data=') . urlencode( $this->json->encode($response) );
			echo file_get_contents( $QRCode );
			
			//$response['status'] = 'Successful';
			//$response['message'] = 'My Profile';
			//$this->api_model->response($this->json->encode($response),200);
	}
}

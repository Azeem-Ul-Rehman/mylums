<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//require('Api.php');

class Customs extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
		$this->load->model('api_model');
	}
	
	public function index()
	{
		
	}
	
	public function interest_industry()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$data['status'] = 'Error';
			$data['message'] = 'Method not match.';
			$this->api_model->response($this->json->encode($data),406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$data['data']['interests'] = $this->interests();
				$data['data']['industry'] = $this->industry();
				$data['status'] = 'Successful';
				$data['message'] = 'List list.';
				$this->api_model->response($this->json->encode($data),200);
			} 
			$data['status'] = 'Error';
			$data['message'] = 'List not found.';
			$this->api_model->response($this->json->encode($data),404);
			
		}
	}
	
	public function interests()
	{
		$arraylist = array();
		//$this->db->where('user_id',$user_id);
		//$this->db->where('interests.id = user_interests.interest_id');
		$ListTypes = $this->db->get('interests');
		$ListTypes = $ListTypes->result_array();
	
		return $ListTypes;
	}
	
	public function industry()
	{
		$arraylist = array();
		//$this->db->where('user_id',$user_id);
		//$this->db->where('industry.id = user_workexperience.industry_id');
		$this->db->from('industry');
		$ListTypes = $this->db->get();
		$ListTypes = $ListTypes->result_array();
	
		return $ListTypes;
	}
}
?>
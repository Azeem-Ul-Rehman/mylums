<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Location extends REST_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model('location_model');
    }
	
	public function index_get()
	{
	
	}
	
	public function index_post()
	{
	
	}
	
	public function countries_get()
	{
		$dataPost = $this->input->post();
		$data = $this->location_model->countries();
		
		$response['data'] = $data;
		$response['status'] = 'Successful';
		$response['message'] = 'Countries List';
		$this->set_response($response, REST_Controller::HTTP_OK);
		return;
	}
	
	public function states_get()
	{
		//$dataPost = $this->input->post();
		$country_id = $this->uri->segment(3);
		
		if($country_id == '')
		{
			$response['status'] = 'Error';
			$response['message'] = 'States not found';
			$this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
			return;
		}
		
		$data = $this->location_model->states($country_id);
		
		$response['data'] = $data;
		$response['status'] = 'Successful';
		$response['message'] = 'States List';
		$this->set_response($response, REST_Controller::HTTP_OK);
		return;
	}
	
	public function cities_get()
	{
		//$dataPost = $this->input->post();
		$state_id = $this->uri->segment(3);
		
		if($state_id == '')
		{
			$response['status'] = 'Error';
			$response['message'] = 'Cities not found';
			$this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
			return;
		}
		
		$data = $this->location_model->cities($state_id);
		
		$response['data'] = $data;
		$response['status'] = 'Successful';
		$response['message'] = 'Cities List';
		$this->set_response($response, REST_Controller::HTTP_OK);
		return;
	}
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Partners Api

require('Api.php');

class Partners extends Api 
{
	public $content_type = "application/json";
	
	public function __construct()
    {
        parent::__construct();
	}
	
	public function index()
	{
	
	}
	
	public function all()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$detailpath = base_url('/partners/detail/');
				$this->db->select('partners.*,
								  CONCAT(\''.$path. '\', image) as imagepath,
								  CONCAT(\''.$detailpath. '\', partners.id) as partnerspath,
								  (select count(*) from partner_offers where partner_id = partners.id) as totaloffers,
								  (select IF (totaloffers > 1, "MultipleDiscount", (select discount from partner_offers where partner_id = partners.id) )) as discount
								  ');
				$this->db->where('partners.id = partner_offers.partner_id');
				$this->db->group_by('partner_offers.partner_id');
				$this->db->from('partners,partner_offers');
				
				if($this->input->get('q'))
				{
					$search = $this->input->get('q');
					$arrays = stringArray($search,' ');
					if(count($arrays) > 0)
					{
						$this->db->group_start();
						foreach($arrays as $arrayRow)
						{
							$this->db->group_start();
							$this->db->or_like('partners.name',$arrayRow); 
							$this->db->or_like('partners.description',$arrayRow);
							$this->db->or_like('partner_offers.discount',$arrayRow);
							$this->db->group_end();
						}
						$this->db->group_end();
					}
				}
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				//$this->db->limit($limit, $offset);
				
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				//$this->db->limit($limit, $offset);
				//var_dump($data2->result_array());
				
				$data['data'] = $data2->result_array();
				
				if(count($data['data']) > 0)
				{
					foreach($data['data'] as &$row)
					{
						if($row['discount'] == 'MultipleDiscount')
						{
							$row['discount'] = 'Multiple Discount';
						}
					}
				}
				
				//var_dump($data['data']);
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'Partners list.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Partners not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
		}
	}
	
	public function nearby()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			$user_id = $this->currentUserId->id;
			$offerradius = get_user_meta($user_id, 'offerradius');
			
			$center_lat 	= $this->input->get('lat'); //$_GET["lat"];
			$center_lng 	= $this->input->get('lng'); //$_GET["lng"];
			$radius 		= $offerradius; //1; //$_GET["radius"];
			
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$query = sprintf("SELECT *, 
								 ( 3959 * acos( cos( radians('%s') ) * cos( radians( partner_offers.lat ) ) * cos( radians( partner_offers.lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( partner_offers.lat ) ) ) ) AS distance 
								 FROM partners,partner_offers 
								 Where partners.id = partner_offers.partner_id
								 HAVING distance < '%s' ORDER BY distance",
								 ($center_lat),
								 ($center_lng),
								 ($center_lat),
								 ($radius));
				//echo $query; 
				$path = base_url('/uploads/');
				$detailpath = base_url('/partners/detail/');
				$this->db->select('*,
								  partners.id as id,
								  partner_offers.id as location_id,
								  (3959 * acos( cos( radians("'. $center_lat.'") ) * cos( radians( partner_offers.lat ) ) * cos( radians( partner_offers.lng ) - radians("'.$center_lng.'") ) + sin( radians("'.$center_lat.'") ) * sin( radians( partner_offers.lat ) ) ) ) AS distance,
								 
								  CONCAT(\''.$path. '\', image) as imagepath,
								  CONCAT(\''.$detailpath. '\', partners.id) as partnerspath');
				$this->db->where('partners.id = partner_offers.partner_id');
				$this->db->from('partners,partner_offers');
				$this->db->group_by('partners.id');
				if($this->input->get('q'))
				{
					$search = $this->input->get('q');
					$arrays = stringArray($search,' ');
					if(count($arrays) > 0)
					{
						$this->db->group_start();
						foreach($arrays as $arrayRow)
						{
							$this->db->group_start();
							$this->db->or_like('partners.name',$arrayRow); 
							$this->db->or_like('partners.description',$arrayRow);
							$this->db->or_like('partner_offers.discount',$arrayRow);
							$this->db->group_end();
						}
						$this->db->group_end();
					}
				}
				
				$this->db->having("distance < '".$radius."'");
				$this->db->order_by('distance');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				//$this->db->limit($limit, $offset);
				
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				//$this->db->limit($limit, $offset);
				//var_dump($data2->result_array());
				
				$data['data'] = $data2->result_array();
				//var_dump($data['data']);
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'Partners list.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Partners not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
		}
	}
	
	public function detail()
	{
		
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$detailpath = base_url('/partners/detail/');
				$this->db->select('*,
								  CONCAT(\''.$path. '\', image) as imagepath');
				
				
				
				$partner_id = $this->uri->segment(3);
				$this->db->where('id', $partner_id);
				$this->db->from('partners');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				$data['data'] = $data2->row_array();
				if(count($data['data']) > 0)
				{
					//$totalCheckedIn = $this->checkedintotal($event_id);
					$interests = $this->offers($partner_id);
					$data['data']['offers'] = $interests;
					//$data['data'][0]['users'] = $this->userstotal($event_id);
					//$data['data'][0]['users']['checkedin'] = $totalCheckedIn['users'];
					//$data['data'][0]['users']['checkedin-total'] = $totalCheckedIn['total'];
					$data['status'] = 'Successful';
					$data['message'] = 'Partner Details.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Partners not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
		
	}
	
	public function nearoffers()
	{
		
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$path = base_url('/uploads/');
				$detailpath = base_url('/partners/detail/');
				$this->db->select('*,
								  CONCAT(\''.$path. '\', image) as imagepath');
				
				
				
				$partner_id = $this->uri->segment(3);
				$this->db->where('id', $partner_id);
				$this->db->from('partners');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				$data['data'] = $data2->row_array();
				if(count($data['data']) > 0)
				{
					$user_id = $this->currentUserId->id;
					$offerradius = get_user_meta($user_id, 'offerradius');
			
					$center_lat 	= $this->input->get('lat'); //$_GET["lat"];
					$center_lng 	= $this->input->get('lng'); //$_GET["lng"];
					$radius 		= $offerradius; //$_GET["radius"];
					//$partner_id = $this->uri->segment(3);
					
					$interests = $this->near_offers_partner($partner_id,$center_lat,$center_lng);
					$data['data']['offers'] = $interests;
					$data['status'] = 'Successful';
					$data['message'] = 'Partner Details.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Partners not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
			
		}
		
	}
	
	public function near_offers_partner($partner_id,$lat,$lng)
	{
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			$user_id = $this->currentUserId->id;
			$offerradius = get_user_meta($user_id, 'offerradius');
			
			$center_lat 	= $lat; //$_GET["lat"];
			$center_lng 	= $lng; //$_GET["lng"];
			$radius 		= $offerradius; //$_GET["radius"];
			$partner_id 	= $partner_id;
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$query = sprintf("SELECT *, 
								 ( 3959 * acos( cos( radians('%s') ) * cos( radians( partner_offers.lat ) ) * cos( radians( partner_offers.lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( partner_offers.lat ) ) ) ) AS distance 
								 FROM partners,partner_offers 
								 Where partners.id = partner_offers.partner_id
								 HAVING distance < '%s' ORDER BY distance",
								 ($center_lat),
								 ($center_lng),
								 ($center_lat),
								 ($radius));
				
				$path = base_url('/uploads/');
				$detailpath = base_url('/partners/detail/');
				
				$this->db->select('partner_offers.*, (3959 * acos( cos( radians("'. $center_lat.'") ) * cos( radians( partner_offers.lat ) ) * cos( radians( partner_offers.lng ) - radians("'.$center_lng.'") ) + sin( radians("'.$center_lat.'") ) * sin( radians( partner_offers.lat ) ) ) ) AS distance');
				$this->db->where('partners.id = partner_offers.partner_id');
				$this->db->where('partners.id', $partner_id);
				$this->db->from('partners,partner_offers');
				
				
				$this->db->having("distance < '".$radius."'");
				$this->db->order_by('distance');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				//$this->db->limit($limit, $offset);
				
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				//$this->db->limit($limit, $offset);
				//var_dump($data2->result_array());
				
				$data['data'] = $data2->result_array();
				return $data['data'];
				//var_dump($data['data']);
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'Partner Offer.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Partners not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
		
	}
	
	public function near_offers()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			$data['data'] = array();			
			$data['status'] = 'Error';
			
			$user_id = $this->currentUserId->id;
			$offerradius = get_user_meta($user_id, 'offerradius');
			
			$center_lat 	= $this->input->get('lat'); //$_GET["lat"];
			$center_lng 	= $this->input->get('lng'); //$_GET["lng"];
			$radius 		= $offerradius; //$_GET["radius"];
			$partner_id = $this->uri->segment(3);
			
			//if($this->input->get('type') && ($this->input->get('parent') or $this->input->get('parent') == 0))
			{
				$query = sprintf("SELECT *, 
								 ( 3959 * acos( cos( radians('%s') ) * cos( radians( partner_offers.lat ) ) * cos( radians( partner_offers.lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( partner_offers.lat ) ) ) ) AS distance 
								 FROM partners,partner_offers 
								 Where partners.id = partner_offers.partner_id
								 HAVING distance < '%s' ORDER BY distance",
								 ($center_lat),
								 ($center_lng),
								 ($center_lat),
								 ($radius));
				//echo $query; 
				$path = base_url('/uploads/');
				$detailpath = base_url('/partners/detail/');
				//CONCAT(\''.$path. '\', image) as imagepath, CONCAT(\''.$detailpath. '\', partners.id) as partnerspath
				$this->db->select('partner_offers.*, (3959 * acos( cos( radians("'. $center_lat.'") ) * cos( radians( partner_offers.lat ) ) * cos( radians( partner_offers.lng ) - radians("'.$center_lng.'") ) + sin( radians("'.$center_lat.'") ) * sin( radians( partner_offers.lat ) ) ) ) AS distance');
				$this->db->where('partners.id = partner_offers.partner_id');
				$this->db->where('partners.id', $partner_id);
				$this->db->from('partners,partner_offers');
				
				/*if($this->input->get('q'))
				{
					$search = $this->input->get('q');
					$arrays = stringArray($search,' ');
					if(count($arrays) > 0)
					{
						$this->db->group_start();
						foreach($arrays as $arrayRow)
						{
							$this->db->group_start();
							$this->db->or_like('partners.name',$arrayRow); 
							$this->db->or_like('partners.description',$arrayRow);
							$this->db->or_like('partner_offers.discount',$arrayRow);
							$this->db->group_end();
						}
						$this->db->group_end();
					}
				}*/
				
				$this->db->having("distance < '".$radius."'");
				$this->db->order_by('distance');
				
				$limit = 10;
				$paged = $this->input->get('paged');
				$offset = ($paged > 0) ? ((int)($paged - 1) * $limit) : 0;
				//$this->db->limit($limit, $offset);
				
				$query = $this->db->get();
				$last_query = $this->db->last_query();
				
				$data['total'] = $query->num_rows();
				$data['pages'] = ceil($query->num_rows()/$limit);
				
				if($this->input->get('all') == 'yes')
				{
					$data2 = $query;
				} else {
					$data2 = $this->db->query($last_query . ' limit '. $offset .','. $limit);
				}
				
				//$this->db->limit($limit, $offset);
				//var_dump($data2->result_array());
				
				$data['data'] = $data2->result_array();
				//var_dump($data['data']);
				if(count($data['data']) > 0)
				{
					$data['status'] = 'Successful';
					$data['message'] = 'Partner Offer.';
				} else {
					$data['status'] = 'Error';
					$data['message'] = 'Partners not found.';
					$this->api_model->response($this->json->encode($data),406);
				}
				$this->api_model->response($this->json->encode($data),200);
			} 
			$this->api_model->response($this->json->encode($data),404);
		}
	}
	
	public function offers($partner_id)
	{
		
		$arraylist = array();
		$this->db->where('partner_id',$partner_id);
		$ListTypes = $this->db->get('partner_offers');
		$ListTypes = $ListTypes->result_array();
		/*if(count($ListTypes) > 0)
		{
			foreach($ListTypes as $ListTypesRow)
			{
				$ListTypesRow['interest'] =  $ListTypesRow['path'];
				$arraylist['interests'][] = $ListTypesRow;
			}
		}*/
		return $ListTypes;
	}
}
?>
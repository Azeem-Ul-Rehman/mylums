<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Preload extends REST_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }
	
	public function index_get()
	{
	
	}
	
	public function index_post()
	{
	
	}
	
	public function verifyemail_post()
	{
		$dataPost = $this->input->post();
		$data = $this->user_model->verify_email($this->input->post('email'),$this->input->post('roll_number'));
		
		if($data)
		{
			if($data['status'] == 'Pending')
			{
				//var_dump($data);
				$response['roll_number'] = $data['roll_number'];
				$response['email'] = $data['personal_email'];
				$response['status'] = 'Successful';
				$response['message'] = 'Email Found';
				$this->set_response($response, REST_Controller::HTTP_OK);
				return;
			} else {
				$response['status'] = 'Error';
				$response['message'] = 'Email already verified';
				$this->set_response($response, REST_Controller::HTTP_OK);
				return;
			}
		} else {
			$response = ['status' => 'Error','message' => 'Email not found'];
			$this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
		}
	}
	
	public function signup_post()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('first_name','First Name', 'required');
		$this->form_validation->set_rules('last_name','Last Name','required');
		
		
		$this->form_validation->set_rules('roll_number','Roll Number', 'required');
		$this->form_validation->set_rules('email','Email','required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('confirmpassword','Confirm Password','required|matches[password]');
		$this->form_validation->set_rules('status','Status','required');
		if($this->form_validation->run() == true)
		{
			$dataPost 			= $this->input->post();
			$roll_number 		= $dataPost['roll_number'];
			$email 				= $dataPost['email'];
			$password 			= $dataPost['password'];
			$confirmpassword 	= $dataPost['confirmpassword'];
			$status				= $dataPost['status'];
			$first_name			= $dataPost['first_name'];
			$last_name			= $dataPost['last_name'];
			
			if($status != 'Successful')
			{
				$response['status'] = 'Error';
				$response['message'] = 'User not verified';
				$this->set_response($response, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
				return;
			}
			
			$data = $this->user_model->create_user($email,$roll_number,$password);
			
			if($data)
			{
				//$tokenData = array();
				//$tokenData['id'] = $data;
				//$response['user'] =  $this->user_model->profile_get($data);
				//$response['user']['image'] =  base_url('/uploads/') .$response['user']['image'];
				//$response['token'] = Authorization::generateToken($tokenData);
				//$this->user_model->update_token($data, $response['token']);
				$userData = array();
				$userData['profile.first_name'] = $first_name;
				$userData['profile.last_name'] = $last_name;
				$userData['profile.personal_email'] = $email;
				$userData['profile.roll_number'] = $roll_number;
				
				
				$this->user_model->profile_update($data,$userData);
				
				$statusEmail = $this->notification->signup($this->user_model->profile_get($data));
				$response['status'] = 'Successful';
				$response['message'] = 'Your account has been successfully created. An email has been sent to you.';
				$this->set_response($response, REST_Controller::HTTP_OK);
				return;
			} else {
				$response['status'] = 'Error';
				$response['message'] = 'Information not correct';
				$this->set_response($response, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
				return;
			}
			
		} else {
			$response['status'] = 'Error';
			$response['message'] = 'Information not correct';
			$this->set_response($response, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
			return;
		}
	}
	
	public function verification_get()
	{
		$userid = $this->uri->segment(2);
		$userid = urldecode($userid);
		$userid = password_decode($userid);
		
		$data = $this->user_model->profile_get($userid);
		if($data)
		{
			$userData = array();
			$userData['users.status'] = 'active';
			$updatedStatus = $this->user_model->profile_update($userid,$userData);
			if($updatedStatus)
			{
				
				$this->notification->signup_thanks($data);
				
				$response['status'] = 'Successful';
				$response['message'] = 'Your account activated, please login and update your profile.';
				$this->set_response($response, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
				return;
			}
		}
		
		$response['status'] = 'Error';
		$response['message'] = 'User not found.';
		$this->set_response($response, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
		return;
	}
	
	public function forgot_password_post()
	{
		$this->load->helper('site');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('email','Email','required|valid_email');
		if($this->form_validation->run() == true)
		{
			$dataPost 			= $this->input->post();
			$email = $dataPost['email'];
			$userData = get_meta('users','email',$email);
			$userData = $userData[0];
			//var_dump($userData);
			
			if($userData)
			{
				if($userData['status'] == 'active')
				{
					$codePassword = rand(1000,1000000);
					$newPassword = password_encode( $codePassword );
					
					
					$userDataUpdate['users.password'] =  $newPassword;
					$this->user_model->profile_update($userData['id'],$userDataUpdate);
					
					$this->notification->reset_password($userData['email'],$codePassword);
					
					$response['status'] = 'Successful';
					$response['message'] = 'your password has been changed successfully, and your new password sent to you in email.';
					$this->set_response($response, REST_Controller::HTTP_OK);
					return;
				} else {
					$response['status'] = 'Error';
					$response['message'] = 'Sorry, your account is inactive.';
					$this->set_response($response, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
					return;
				}
			} else {
				$response['status'] = 'Error';
				$response['message'] = 'Your email address not found.';
				$this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
				return;
			}		
		} else {
			$response['status'] = 'Error';
			$response['message'] = 'Please enter a valid email address.';
			$this->set_response($response, REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
			return;
		}
	}
}
?>
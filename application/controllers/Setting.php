<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Setting Api

require('Api.php');

class Setting extends Api 
{
	public $content_type = "application/json";
	
	public function __construct()
    {
        parent::__construct();
		//define('ENVIRONMENT', 'development');
		//$this->load->model('messages_model');
	}

	public function index()
	{
		
	}
	
	public function get()
	{
		if ($this->input->server('REQUEST_METHOD') != 'GET')
        {
			$this->api_model->response('',406);
		} else {
			//get_user_meta
			$user_id = $this->currentUserId->id;
			$offerradius = get_user_meta($user_id, 'offerradius');
			$mute = get_user_meta($user_id, 'mute');
			$location = get_user_meta($user_id, 'location');
			
			$data['setting']['mute'] = $mute;
			$data['setting']['location'] = $location;
			$data['setting']['offerradius'] = $offerradius;
			
			$data['status'] = 'Successful';
			$data['message'] = 'Setting get.';
			$this->api_model->response($this->json->encode($data),200);
		}
	}
	
	public function update()
	{
		if ($this->input->server('REQUEST_METHOD') != 'POST')
        {
			$this->api_model->response('',406);
		} else {
			$user_id = $this->currentUserId->id;
			
			
			$mute = $this->input->post('mute');
			$location = $this->input->post('location');
			$offerradius = $this->input->post('offerradius');
			
			if($mute)
			{
				update_user_meta($user_id, 'mute' , $mute);
			}
			
			if($location)
			{
				update_user_meta($user_id, 'location' , $location);
			}
			
			if($offerradius)
			{
				update_user_meta($user_id, 'offerradius' , $offerradius);
			}
			
			$data['status'] = 'Successful';
			$data['message'] = 'Setting updated.';
			$this->api_model->response($this->json->encode($data),200);
		}	
	}
}
?>
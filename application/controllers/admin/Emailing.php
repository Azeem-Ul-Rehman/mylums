<?php
class Emailing extends CI_Controller 
{

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/emailing';
 	public $columns;
	public $columnsEdit;
	
	/**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
		$this->load->model('events_model');
		$this->load->model('user_model');
		$this->load->model('notification_model');
		if(!$this->session->userdata('is_logged_in'))
		{
            redirect('admin/login');
        }
		
		$columns = array();
		$columns['name'] = 'Name';
		//$columns['description'] = 'Description';
		$columns['dates'] = 'Date';
		$columns['start_time'] = 'Start Time';
		$columns['end_time'] = 'End Time';
		//$columns['members'] = 'Members';
		$columns['status'] = 'Status';
		
		$this->columns = $columns;
		
	$columnsEdit = array();
	$columnsEdit[] = array('field' =>'name','database' => 'name','title' => 'Name', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'description','database' => 'description','title' => 'Description', 'status' => '','type' => 'textarea');
	$columnsEdit[] = array('field' =>'dates','database' => 'dates','title' => 'Date', 'status' => 'required','type' => 'date');
	$columnsEdit[] = array('field' =>'start_time','database' => 'start_time','title' => 'Start time', 'status' => 'required','type' => 'time');
	$columnsEdit[] = array('field' =>'end_time','database' => 'end_time','title' => 'End time', 'status' => 'required','type' => 'time');
	$columnsEdit[] = array('field' =>'venue','database' => 'venue','title' => 'Venue', 'status' => 'required','type' => '');
	$columnsEdit[] = array('field' =>'geo_location','database' => 'geo_location','title' => 'Geo Location', 'status' => 'required','type' => 'location');
	$columnsEdit[] = array('field' =>'status','database' => 'status','title' => 'Status', 'status' => 'required','type' => '');
	
	$this->columnsEdit = $columnsEdit;
    }
	
	
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
		$event_id = $this->uri->segment(4);
		$url = $this->uri->segment(2);
		
		$users = $this->input->post('users');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		if($users)
		{
			$this->add_users($users,$subject,$message);
			$this->session->set_flashdata('flash_message', 'updated');
		}
		
		//$data['manufacture'] = $this->events_model->get_manufacture_by_id($event_id);
		//$data['images'] = $this->events_model->get_gallery($id);
        $directory = $this->uri->segment(2);
		$data['controller']=$this; 
        $data['users'] = $this->get_users();
		$data['main_content'] = 'admin/'.$directory.'/invites';
        $this->load->view('includes/template', $data);
	}//index

    public function add()
    {
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
           if(count($this->columnsEdit) > 0) 
			{ 
				$feildCounter = 0;
				foreach($this->columnsEdit as $columnRow)
				{
					$this->form_validation->set_rules($columnRow['field'], $columnRow['title'], $columnRow['status']);
					$feildCounter++;
				}
			}
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
				
				
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
				//$config['max_size']	= '100';
				//$config['max_width']  = '1024';
				//$config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				$data['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('image'))
				{
					$data['error'] = $this->upload->display_errors();
				} else {
					$data['upload_data'] = $this->upload->data();
				}
				
				if($data['upload_data']['file_name'])
				{
					$data_to_store['photo'] = $data['upload_data']['file_name'];
				}
				
				if(count($this->columnsEdit) > 0) 
				{ 
					foreach($this->columnsEdit as $column)
					{
						if($column['type'] == 'password')
						{
							if($this->input->post($column['field']) !="")
							{
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = password_encode($this->input->post($column['field']));
							}
						} else {
							if($column['type'] != 'disable')
							{
								//$this->form_validation->set_rules($column, $vals, 'required');
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = $this->input->post($column['field']);
							}
						}
					}
				}
				
				if($this->input->post('geo_locationLat'))
				{
					$data_to_store['lat'] = $this->input->post('geo_locationLat');
				}
				
				if($this->input->post('geo_locationLon'))
				{
					$data_to_store['lon'] = $this->input->post('geo_locationLon');
				}
				
				$data_to_store['full_time'] = strtotime($this->input->post('dates') . ' ' . $this->input->post('start_time'));
				
				$data_to_store['created_at'] = time();
				
                //if the insert has returned true then we show the flash message
                if($this->events_model->store_manufacture($data_to_store))
				{
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
		//$data['allcategories'] = $this->categories_model->get_manufacturers();
		$data['columns'] = $this->columns;
		$data['columnsEdit'] = $this->columnsEdit;
		
		$directory = $this->uri->segment(2);
        $data['main_content'] = 'admin/'.$directory.'/add';
        $this->load->view('includes/template', $data);
    }       

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
            //$this->form_validation->set_rules('name', 'Name', 'required');
			//$this->form_validation->set_rules('description', 'Description', 'required');
			if(count($this->columnsEdit) > 0) 
			{ 
				$feildCounter = 0;
				foreach($this->columnsEdit as $columnRow)
				{
					$this->form_validation->set_rules($columnRow['field'], $columnRow['title'], $columnRow['status']);
					$feildCounter++;
				}
			}
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    			
				$config['upload_path'] = './uploads/';
				$config['allowed_types'] = '*';
				$config['encrypt_name'] = TRUE;
				//$config['max_size']	= '100';
				//$config['max_width']  = '1024';
				//$config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				$data['upload_data']['file_name'] = NULL;
				if ( ! $this->upload->do_upload('image'))
				{
					$data['error'] = $this->upload->display_errors();
				} else {
					$data['upload_data'] = $this->upload->data();
				}
				
				if($data['upload_data']['file_name'])
				{
					$data_to_store['photo'] = $data['upload_data']['file_name'];
				}
				
				if(count($this->columnsEdit) > 0) 
				{ 
					foreach($this->columnsEdit as $column)
					{
						if($column['type'] == 'password')
						{
							if($this->input->post($column['field']) !="")
							{
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = password_encode($this->input->post($column['field']));
							}
						} else {
							if($column['type'] != 'disable')
							{
								//$this->form_validation->set_rules($column, $vals, 'required');
								$UpdatedName = str_replace("___",".",$column['field']);
								$data_to_store[$UpdatedName] = $this->input->post($column['field']);
							}
						}
					}
				}
				
				if($this->input->post('geo_locationLat'))
				{
					$data_to_store['lat'] = $this->input->post('geo_locationLat');
				}
				
				if($this->input->post('geo_locationLon'))
				{
					$data_to_store['lon'] = $this->input->post('geo_locationLon');
				}
				$data_to_store['full_time'] = strtotime($this->input->post('dates') . ' ' . $this->input->post('start_time'));
				
				$data_to_store['updated_at'] = time();
				//$data_to_store['profile.updated_at'] = time();
				//var_dump($data_to_store);
				//die();
                //if the insert has returned true then we show the flash message
				
                if($this->events_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
				$directory = $this->uri->segment(2);
                redirect('admin/'.$directory.'/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
		$data['columns'] = $this->columns;
		$data['columnsEdit'] = $this->columnsEdit;
        $data['manufacture'] = $this->events_model->get_manufacture_by_id($id);
        $directory = $this->uri->segment(2);
        $data['main_content'] = 'admin/'.$directory.'/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
		$url = $this->uri->segment(2);
        $this->events_model->delete_manufacture($id);
        redirect('admin/' . $url);
		//edit
    }
	

	/*
	public function invites()
	{
		$event_id = $this->uri->segment(4);
		$url = $this->uri->segment(2);
		
		$users = $this->input->post('users');
		if($users)
		{
			$this->add_users($users);
			$this->session->set_flashdata('flash_message', 'updated');
		}
		
		$data['manufacture'] = $this->events_model->get_manufacture_by_id($event_id);
		//$data['images'] = $this->events_model->get_gallery($id);
        $directory = $this->uri->segment(2);
		$data['controller']=$this; 
        $data['users'] = $this->get_users();
		$data['main_content'] = 'admin/'.$directory.'/invites';
        $this->load->view('includes/template', $data);
	}
	*/
	
	function get_users()
	{
		$arraylist = array();
		$this->db->select('*');
		$this->db->where('user_type != \'admin\'');
		$this->db->where('status','active');
		$this->db->where('profile.user_id=users.id');
		$this->db->from('users,profile');
		$ListTypes = $this->db->get();
		//echo $this->db->last_query();
		$ListTypes = $ListTypes->result_array();
		return $ListTypes;
	}
	
	function add_users($data,$subject,$message)
	{
		//var_dump($data);
		if(count($data) > 0)
		{
			foreach($data as $row)
			{
				$user_info = $this->user_model->profile_get($row);
				$to = $user_info['email'];
				$this->notification_model->massemailing($to,$subject,$message);
			}
		}
	}
}
?>
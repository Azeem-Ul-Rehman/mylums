<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function password_encode($string = '')
{
	$CI 	= CI_Controller::get_instance();
	return $CI->encryption->encrypt($string);
}

function password_decode($string = '')
{
	$CI 	= CI_Controller::get_instance();
	return $CI->encryption->decrypt($string);
}

function getLocality($addresses)
{
	$types 		= array_column($addresses, 'types');
	foreach($types as $i => $type)
	{
		if(in_array('locality', $type))
			return $addresses[$i];
	}
}

function stringArray($string,$values = ' ')
{
	$string  = $string;
	$data = explode($values, $string);
	return $data;
}
<?php
class Alumni_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
	
	public $Table = 'users,profile';
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_id 
    * @return array
    */
    public function get_manufacture_by_id($id)
    {
		$this->db->select('*');
		$this->db->where('users.id', $id);
		$this->db->where('users.user_type != "admin"');
		$this->db->where('users.id = profile.user_id');
		$this->db->from($this->Table);
		$query = $this->db->get();
		return $query->result_array(); 
    }    

    /**
    * Fetch manufacturers data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_manufacturers($search_string=null, $order=null, $order_type='Asc', $limit_start=null, $limit_end=null)
    {
	    
		$this->db->select('*');
		$this->db->where('users.user_type != "admin"');
		$this->db->where('users.id = profile.user_id');
		$this->db->from($this->Table);
		
		if($search_string && $this->input->post('searchField'))
		{
			$searchField = $this->input->post('searchField');
			$this->db->like($searchField, $search_string);
		}
		
		$this->db->group_by('users.id');

		if($order){
			$this->db->order_by($order, $order_type);
		}else{
		    $this->db->order_by('users.id', $order_type);
		}

        if($limit_start && $limit_end){
          $this->db->limit($limit_start, $limit_end);	
        }
		
		$category = $this->session->userdata('category');
		if($category)
		{
			$this->db->where('CatID',$category);
		}

        if($limit_start != null){
          $this->db->limit($limit_start, $limit_end);    
        }
        
		$query = $this->db->get();
		
		return $query->result_array(); 	
    }

    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
    function count_manufacturers($search_string=null, $order=null)
    {
		$this->db->select('*');
		$this->db->where('users.user_type != "admin"');
		$this->db->where('users.id = profile.user_id');
		$this->db->from($this->Table);
		
		if($search_string && $this->input->post('searchField'))
		{
			$searchField = $this->input->post('searchField');
			$this->db->like($searchField, $search_string);
		}
		
		if($order){
			$this->db->order_by($order, 'Asc');
		}else{
		    $this->db->order_by('users.id', 'Asc');
		}
		
		$category = $this->session->userdata('category');
		if($category)
		{
			$this->db->where('CatID',$category);
		}
		
		$query = $this->db->get();
		return $query->num_rows();        
    }

    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert($this->Table, $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($id, $data)
    {
		$querys = '';
		$total = count($data);
		$NewCount = 0;
		if(count($data) > 0)
		{
			foreach($data as $keys => $valus)
			{
				$NewCount++;
				if($NewCount == $total)
				{
					$querys .= $keys . ' = ' . '"'.$valus.'" ';
				} else {
					$querys .= $keys . ' = ' . '"'.$valus.'", ';
				}
			}
		}
		$querysdd = $this->db->query('UPDATE users, profile set '. $querys .' where users.id = ' . $id . ' and profile.user_id = ' .$id);
		//echo $this->db->last_query();
		//var_dump($query->result_array());
		//die;
		//$this->db->set($data);
		//$this->db->where('profile.user_id', $id);
		//$this->db->where('users.id', $id);
		//$this->db->from('users as, profile as profile');
		//$this->db->update('users as users,profile as profile',$data);
		
		//$this->db->where('user_id', $id);
		//$this->db->update('profile', $dataA);
		return $this->db->affected_rows();
			
		
	}

    /**
    * Delete manufacturer
    * @param int $id - manufacture id
    * @return boolean
    */
	function delete_manufacture($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('users');
		
		$this->db->where('user_id', $id);
		$this->db->delete('profile');
	}
	
	function verify_insert($data)
	{
		$this->db->select('*');
		$this->db->where('roll_number', $data['Roll Number']);
		$this->db->from('preload');
		$query = $this->db->get();
		$checked = $query->row_array();
		if($checked)
		{
			return false;
		} else {
			$insert = array();
			$insert['roll_number'] = $data['Roll Number'];
			$insert['full_name'] = $data['Full Name'];
			$insert['program'] = $data['Program'];
			$insert['major'] = $data['Major'];
			$insert['minor'] = $data['Minor'];
			$insert['graduation_year'] = $data['Graduation Year'];
			$insert['personal_email'] = $data['Personal Email'];
			$insert['official_email'] = $data['Official Email'];
			$insert['campus_email'] = $data['Campus Email'];
			$insert['alternate_email'] = $data['Alternate Email'];
			$insert['landline'] = $data['Landline'];
			$insert['alternate_landline'] = $data['Alternate Landline'];
			$insert['mobile'] = $data['Mobile'];
			$insert['alternate_mobile'] = $data['Alternate Mobile'];
			$insert['current_address'] = $data['Current Address'];
			$insert['current_city'] = $data['Current City'];
			$insert['current_state'] = $data['Current State'];
			$insert['current_country'] = $data['Current Country'];
			$insert['permanent_address'] = $data['Permanent Address'];
			$insert['permanent_city'] = $data['Permanent City'];
			$insert['permanent_state'] = $data['Permanent State'];
			$insert['permanent_country'] = $data['Permanent Country'];
			$insert['business_address'] = $data['Business Address'];
			$insert['business_city'] = $data['Business City'];
			$insert['business_state'] = $data['Business State'];
			$insert['business_country'] = $data['Business Country'];
			$insert['current_experience_company'] = $data['Current Company'];
			$insert['current_experience_designation'] = $data['Current Designation'];
			$insert['current_experience_department'] = $data['Current Department'];
			$insert['current_experience_industry'] = $data['Current Industry'];
			$insert['status'] = 'Pending';
			$insert['created_at'] = time();
			$this->db->insert('preload', $insert);
			$id = $this->db->insert_id();
			return $id;
		}
	}
}
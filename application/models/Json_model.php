<?php

class Json_model extends CI_Model 
{
	public function encode($data)
	{
		if(is_array($data))
		{
			return json_encode($data);
		}
	}
	
	public function decode($data)
	{
		return json_decode($data,true);
	}
}
?>
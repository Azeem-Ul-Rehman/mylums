<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messages_model extends CI_Model
{
	public function all($id)
	{
		return $this->db->get_where('todo', ['user_id' => $id])->result();
	}

	public function get($id) 
	{
		return $this->db->get_where('todo', ['id' => $id])->row();
	}

	public function create(array $post)
	{
		/*$data = [
		'user_id' => $token->id,
		'todo' => $post['todo'],
		'done' => $post['done'],
		];*/
		$this->db->insert('chat_messages', $post);
		return $this->db->insert_id();
	}
   
	public function update_chatroom_time($room_id)
	{
		$data = array('updated_at' => time());	
		$this->db->where('id', $room_id);
		$this->db->update('chats', $data);
	}
	
	public function updatedthreadsstatus($room_id,$status)
	{
		$data = array('updated_at' => time(),'status' => $status);	
		$this->db->where('id', $room_id);
		return $this->db->update('chats', $data);
	}
	
	public function updatedarchive($chatRoomID,$user_id,$status)
	{
		$this->db->select('*');
		$this->db->where('id',$chatRoomID);
		$this->db->from('chats');
		$query = $this->db->get();
		$query = $query->row_array();
		if($query['archive'] == '')
		{
			$data = array('updated_at' => time(),'archive' => $user_id);	
			$this->db->where('id', $chatRoomID);
			return $this->db->update('chats', $data);
		} else {
			if($status == 'unarchive')
			{
				$unarchive = 0;
				if($query['receiver_id'] == $user_id)
				{
					$unarchive = $query['sender_id'];
				}
				if($query['sender_id'] == $user_id)
				{
					$unarchive = $query['receiver_id'];
				}
				
				if($query['archive'] == 'both')
				{
					$data = array('updated_at' => time(),'archive' => $unarchive);	
					$this->db->where('id', $chatRoomID);
					return $this->db->update('chats', $data);
				} elseif($query['archive'] == $user_id)
				{
					$data = array('updated_at' => time(),'archive' => '');	
					$this->db->where('id', $chatRoomID);
					return $this->db->update('chats', $data);
				}
			} else {		
				if($query['archive'] != $user_id)
				{
					$data = array('updated_at' => time(),'archive' => 'both');	
					$this->db->where('id', $chatRoomID);
					return $this->db->update('chats', $data);
				}
			}
		}
		//var_dump($query);
	}
}
?>
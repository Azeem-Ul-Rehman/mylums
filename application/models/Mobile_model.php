<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobile_Model extends CI_Model
{
	protected $table = 'devices';
	
	public function registered($userid)
	{
		$condition = ['user_id' => $userid];
        $device = $this->db->get_where($this->table, $condition)->row_array();
		return $device;
	}
	
	public function register($data)
	{
		$status = $this->registered($data['user_id']);
		
		if($status)
		{
			//return false;
			$data = array(
			'user_id' => $data['user_id'],
			'fcm_token' => $data['fcm_token'],
			'type' => $data['type'],
			'created_at' => time()
			);
			$this->db->where('user_id', $data['user_id']);
			$data = $this->db->update($this->table, $data);
			return $data;// $this->db->insert_id();
			
		} else {
			$data = array(
			'user_id' => $data['user_id'],
			'fcm_token' => $data['fcm_token'],
			'type' => $data['type'],
			'created_at' => time()
			);
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}
	}
}
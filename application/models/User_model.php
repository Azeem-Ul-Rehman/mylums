<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model
{
	protected $table = 'users';
	
    public function login($username, $password)
    {
        $condition = ['email' => $username,'status'=>'active'];
        $user = $this->db->get_where($this->table, $condition)->row_array();
		if(password_decode($user['password']) == password_decode($password))
		{
			return $user;
		} else {
			return false;
		}
    }
	
	public function verify_token($token)
	{
		
        $condition = ['token' => $token];
        $user = $this->db->get_where($this->table, $condition)->row_array();
		return $user;
	}
	
	public function update_token($user_id,$token)
	{
		$this->db->set('token', $token);
		$this->db->set('updated_at', time());
		$this->db->where('id', $user_id);
		$this->db->update($this->table);
	}
	
	public function verify_email($email = NULL,$rollnumber = NULL)
	{
		$this->db->select('roll_number, personal_email, status');
		$this->db->where('1=1');
		if($email)
		{
			$this->db->where('personal_email',$email);
		}
		
		if($rollnumber)
		{
			$this->db->where('roll_number',$rollnumber);
		}
		
		$query = $this->db->get('preload');
		//echo $this->db->last_query();
		$data = $query->row_array();
		//var_dump($data);
		if($data)
		{
			return $data;
		} else {
			return false;
		}
	}
	
	public function create_user($email,$rollno,$password)
	{
		$data = array(
		'email' => $email,
		'password' => password_encode($password),
		'roll_number' => $rollno,
		'status' => 'inactive',
		'created_at' => time()
		);
		
		$this->db->insert($this->table, $data);
		$userID = $this->db->insert_id();
		$this->profile_create($userID);
		return $userID;
	}
	
	public function profile_create($userID)
	{
		$data = array(
		'user_id' => $userID,
		'created_at' => time()
		);
		$this->db->insert('profile', $data);
	}
	
	public function profile_get($userID)
	{
		//$this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*');
		$path = base_url('/uploads/');
		$profile = base_url('/alumni/profile/');
		$qrcode = base_url() .'users/profile/qrcode/';
		$this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*, 
						  CONCAT(\''.$qrcode. '\', users.id) as qrcode,
						  CONCAT(\''.$path. '\', profile.image) as imagepath, 
						  CONCAT(\''.$profile. '\', users.id) as profilepath');
		$this->db->where('users.id = profile.user_id');
		$this->db->where('user_id',$userID);
		$query = $this->db->get('users,profile');
		
		
		/*$path = base_url('/uploads/');
		$profile = base_url('/alumni/profile/');
		$qrcode = base_url() .'users/profile/qrcode/';
		$this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*, 
						  CONCAT(\''.$qrcode. '\', users.id) as qrcode,
						  CONCAT(\''.$path. '\', profile.image) as imagepath, 
						  CONCAT(\''.$profile. '\', users.id) as profilepath');*/
		//$this->db->select('users.id as user_id, users.email as email,users.roll_number as roll_number,status, profile.*');
		//$this->db->where('users.id = profile.user_id');
		//$this->db->where('users.user_type != "admin"');
		//$this->db->where('users.status', "active");
		//$query = $this->db->from('users,profile');
		
		
		$data = $query->row_array();
		if($data)
		{
			return $data;
		} else {
			return false;
		}
	}
	
	public function profile_update($userID, $data)
	{
		$querys = '';
		$total = count($data);
		$NewCount = 0;
		if(count($data) > 0)
		{
			foreach($data as $keys => $valus)
			{
				$NewCount++;
				if($NewCount == $total)
				{
					$querys .= $keys . ' = ' . '"'.$valus.'" ';
				} else {
					$querys .= $keys . ' = ' . '"'.$valus.'", ';
				}
			}
		}
		$querysdd = $this->db->query('UPDATE users, profile set '. $querys .' where users.id = ' . $userID . ' and profile.user_id = ' .$userID);
		return $this->db->affected_rows();
	}
	
	public function interests($user_id)
	{
		$arraylist = array();
		
		$this->db->where('user_id',$user_id);
		$this->db->where('interests.id = user_interests.interest_id');
		$ListTypes = $this->db->get('user_interests,interests');
		$ListTypes = $ListTypes->result_array();
	
		return $ListTypes;
	}
	
	public function workexperience($user_id)
	{	
		$arraylist = array();
		
		$this->db->where('user_id',$user_id);
		$this->db->where('industry.id = user_workexperience.industry_id');
		$this->db->from('user_workexperience,industry');
		$ListTypes = $this->db->get();
		$ListTypes = $ListTypes->result_array();
		//echo $this->db->last_query();
		return $ListTypes;
	}
	
	public function linkedin($user_id)
	{
		$arraylist = array();
		$this->db->where('user_id',$user_id);
		$ListTypes = $this->db->get('linkedin');
		$ListTypes = $ListTypes->row_array();
		$ListTypes['userinfo'] =  $this->json->decode($ListTypes['userinfo']);
		return $ListTypes;
	}
	
	public function logout($userID)
	{
		$this->update_token($userID,'');
		
		$this->db->where('user_id',$userID);
		$this->db->delete('devices');
	}
	
	public function get_user_object($user_id)
	{
		$user 				= $this->user_model->profile_get($user_id);
		
		if(!$user)
		{
			return false;
		}
		
		$interests 			= $this->user_model->interests($user_id);
		$workexperience 	= $this->user_model->workexperience($user_id);
		$preload 			= $this->preload($user['email']);
		$linkedin 			= $this->user_model->linkedin($user_id);
		$setting 			= $this->get_setting($user_id);
		$homescreen 		= $this->homescreen_get('no');
		
		
		//$tokenData = array();
		//$tokenData['id'] 					= $user['id'];
		$response['user'] 					= $user;
		$response['user']['interests'] 		= $interests;
		$response['user']['workexperience'] = $workexperience;
		
		$response['user']['proload'] 		= $preload;
		$response['user']['linkedin'] 		= $linkedin;
		$response['user']['qrcode'] 		= base_url() .'users/profile/qrcode/' . $user_id . '.png';
		$response['user']['imagepath'] 		= base_url('/uploads/') .$response['user']['image'];
		$response['user']['setting'] 		= $setting;
		$response['homescreen'] 			= $homescreen;
		return $response;
	}
	
	public function preload($data)
	{
		$arraylist = array();
		$this->db->where('personal_email',$data);
		$ListTypes = $this->db->get('preload');
		$ListTypes = $ListTypes->row_array();
		return $ListTypes;
	}
	
	public function get_setting($user_id)
	{
		$user_id = $user_id; //$this->currentUserId->id;
		$offerradius = get_user_meta($user_id, 'offerradius');
		$mute = get_user_meta($user_id, 'mute');
		$location = get_user_meta($user_id, 'location');
		
		$data['mute'] = $mute;
		$data['location'] = $location;
		$data['offerradius'] = $offerradius;
		return $data; 
	}
	
	public function homescreen_get($output = NULL)
	{
		$arraylist = array();
		$this->db->where('option_name','homescreen');
		$ListTypes = $this->db->get('options');
		$ListTypes = $ListTypes->row_array();
		$path = base_url('/uploads/') . $ListTypes['option_value'];
		
		if($output == 'no')
		{
			return $path;
		} else {
			$response['homescreen'] = $path;
			$response['status'] = 'Successful';
			$this->set_response($response, REST_Controller::HTTP_OK);
		}
	}
	
	function get_fcm($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->from('devices');
		$this->db->get();
		$data = $this->db->row_array();
		var_dump($data);
		return $data['fcm_token'];
	}
}

<div class="content">
  <div class="row">
    <ul class="breadcrumb">
      <li><a href="<?php echo site_url("admin"); ?>"> <?php echo ucfirst($this->uri->segment(1));?> </a></li>
      <li class="active"> <?php echo ucfirst($this->uri->segment(2));?> </li>
    </ul>
    <div class="page-header users-header">
      <h2> <?php echo ucfirst($this->uri->segment(2));?> <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Add a new</a> </h2>
    </div>
  </div>
</div>
<div class="col-lg-12">
  <div class="block">
    <div class="block-content">
      <div class="well">
           
            <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            //save the columns names in a array that we will use as filter         
            $options_manufacturers = array();    
            foreach ($manufacturers as $array) {
              foreach ($array as $key => $value) {
                $options_manufacturers[$key] = $key;
              }
              break;
            }

            echo form_open('admin/categories', $attributes);
     
              echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected);

              echo form_label('Order by:', 'order');
              echo form_dropdown('order', $options_manufacturers, $order, 'class="span2"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span1"');

              echo form_submit($data_submit);

            echo form_close();
            ?>

          </div>
      <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th class="header">#</th>
            <th class="yellow header headerSortDown">Image</th>
            <th class="yellow header headerSortDown">Name</th>
            <th class="yellow header headerSortDown">Type</th>
            <th class="header">Parent</th>
            <th class="header"></th>
          </tr>
        </thead>
        <tbody>
          <?php
              foreach($manufacturers as $row)
              {
                echo '<tr>';
                echo '<td>'.$row['ID'].'</td>';
				if($row['C_Path']){
					echo '<td><img src="'.base_url().'uploads/'.$row['C_Path'].'" name="'.$row['C_Name'].'" alt="'.$row['C_Name'].'" width="100" /></td>';
				} else {
					echo '<td></td>';
				}
				echo '<td>'.$row['C_Name'].'</td>';
				echo '<td>'. ucfirst($row['C_Type']).'</td>';
				echo '<td>';
				if($row['ParentId'] > 0)
				{
					$Parent = $this->categories_model->get_manufacture_by_id($row['ParentId']);
					echo $Parent[0]['C_Name'];
				}
				
				
				echo '</td>';
                echo '<td class="crud-actions">
                  <a href="'.site_url("admin").'/'.$this->uri->segment(2).'/update/'.$row['ID'].'" ><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>';
				  if($row['ID'] != 1)
				  {
                  echo ' <a href="#." onclick="confirmDelete(\''.site_url("admin").'/'.$this->uri->segment(2).'/delete/'.$row['ID'].'\')" ><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button></a>';
				  }
                echo '</td>';
                echo '</tr>';
              }
              ?>
        </tbody>
      </table>
     
    </div>
     <?php //echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>
      <div class="block-content">
      <nav class="text-right"> <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?> </nav>
      </div>
  </div>
</div>

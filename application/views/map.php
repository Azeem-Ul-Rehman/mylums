<style>
.pac-container.pac-logo
{
	z-index:999999999;
}
</style>
<script type="text/javascript">
function openMap(myid)
{
	$('#'+myid +'Modal').modal();
	initMap(myid);
}

var GoogleMapID;
function initMap(MapID)
{
	var fieldId = "location-query";
	var mapId   = MapID + 'Map';
	GoogleMapID = MapID;
	var input   = document.getElementById(fieldId);
	var options = { componentRestrictions: {country: 'pk'}, types: ['establishment'] };

	var mapOpt      = { zoom: 10 };
	var myLatLng    = {lat: 31.4848634, lng: 74.3830406};
	var markerTitle = "";
	var showMarker  = false;

	options.bounds    = new google.maps.LatLngBounds(new google.maps.LatLng(31.4848313, 74.3830971), new google.maps.LatLng(31.4848313, 74.3830971));
	var myLatLng    = { lat: 31.4848313, lng: 74.3830971 };
	mapOpt.center   = myLatLng;
	showMarker      = true;
	markerTitle     = "Lahore, Punjab, Pakistan";
	map	= new google.maps.Map(document.getElementById(mapId), mapOpt);

  if(showMarker)
  {
	marker  = new google.maps.Marker({
	  position:   myLatLng,
	  anchorPoint: new google.maps.Point(0, -29),
	  draggable:  true,
	  animation:  google.maps.Animation.DROP,
	  title:      markerTitle
	});
	marker.setMap(map);
	map.setZoom(14);
	marker.addListener('click', toggleBounceMarker);
	marker.addListener('dragend', markerDragged);
  }

  autocomplete = new google.maps.places.Autocomplete(input, options);
  autocomplete.bindTo('bounds', map);

  autocomplete.addListener('place_changed', function () {
	marker.setVisible(false);
	var place = autocomplete.getPlace();
	uuu=place;
	var data  = {
				title: $('#' + fieldId).val(), 
				latitude: place.geometry.location.lat(), 
				longitude: place.geometry.location.lng()
			  };

	if (!place.geometry) 
	{
	  // User entered the name of a Place that was not suggested and
	  // pressed the Enter key, or the Place Details request failed.
	  window.alert("No details available for input: '" + place.name + "'");
	  return;
	}

	if (place.geometry.viewport) 
	{
	  map.fitBounds(place.geometry.viewport);
	} 
	else 
	{
	  map.setCenter(place.geometry.location);
	  map.setZoom(17);  // Why 17? Because it looks good.
	}
	
	marker.setPosition(place.geometry.location);
	marker.setVisible(true);
	
	$('#'+GoogleMapID).val($('#' + fieldId).val());
	$('#'+GoogleMapID+'Lat').val(place.geometry.location.lat());
	$('#'+GoogleMapID+'Lon').val(place.geometry.location.lng());
	
	//alert('auto complete');
	//alert(GoogleMapID);
	setTimeout(function()
	{
	  //updateLocation(data);
	}, 1000);
  });
}

function toggleBounceMarker()
{
	if (marker.getAnimation() !== null) 
	{
	marker.setAnimation(null);
	} 
	else 
	{
	marker.setAnimation(google.maps.Animation.BOUNCE);
	}
}

function updateLocation(data, triggerAutocomplete = false)
{
  $.ajax({
	type: "POST",
	url: "<?php echo site_url("admin"); ?>/events/setlocation",
	data: {
	  location_title: data.title,
	  location_lat:   data.latitude,
	  location_lon:   data.longitude
	},
	dataType: "json",
	success: function(response)
	{
	  locationUpdated = true;
	  if(response.status == 'success')
	  {
		if(triggerAutocomplete)
		{
		  myLatLng    = new google.maps.LatLng(response.data.location_lat, response.data.location_lon);
		  $('#location-query').val(response.data.user_location_full);
		  map.setCenter(myLatLng);
		  map.setZoom(17);
		  geoMarkerSet(myLatLng);
		}

		$('button[name="location"]').html('<i class="fas fa-map-marker-alt"></i> ' + response.data.user_location);
		$('button[name="location"]').attr('data-user-location-full', response.data.user_location_full);
		$('button[name="location"]').attr('data-location-lat', response.data.location_lat);
		$('button[name="location"]').attr('data-location-lon', response.data.location_lon); 
		
		$('#'+GoogleMapID).val(response.data.user_location_full);
		$('#'+GoogleMapID+'Lat').val(response.data.location_lat);
		$('#'+GoogleMapID+'Lon').val(response.data.location_lon);
		
		//alert(GoogleMapID);
		//alert('changed location');
		
		<?php /*?>if(!$.magnificPopup.instance.isOpen)
		{
				setTimeout(function()
				{
					if(localStorage.getItem("reloadStop") == 'Yes')
					{
						localStorage.setItem("reloadStop", "No");
						locationUpdate();
					} else {
						location.reload();
					}
				}, 1000);
		}<?php */?>           
	  }
	  else
	  {
		alert(response.message);
	  }
	},
	error: function(response)
	{
	  alert("Unable to Update Location.");
	}
  });
}	

function markerDragged(e)
{
	console.log(e);
	//lat log {title: "", latitude: e.latLng.lat(), longitude: e.latLng.lng()}
	updateLocation({title: "", latitude: e.latLng.lat(), longitude: e.latLng.lng()}, true);
}

function geoMarkerSet(geometry)
{
	marker.setVisible(false);
	marker.setPosition(geometry);
	marker.setVisible(true);
}	
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSozsDOYhjtqX8apR0AydJDDJZaI5ACO0&libraries=places&callback=initMap"></script>